import os
import csv
import random
import shutil

from api.models import CarMake, CarModel, CarOffer, year_choices
from django.core.management.base import BaseCommand
from django.conf import settings


car_file_names = [
    os.path.join(settings.BASE_DIR, 'api', 'data', 'car_images', f'car_img_{n}.jpg')
    for n in range(1, 11)
]


class Command(BaseCommand):
    default_new_offers = 100

    def handle(self, *args, **options):
        relative_path = ['api', 'data', 'car_models.csv']
        path = os.path.join(*relative_path)
        with open(path, encoding='latin-1') as f:
            reader = csv.reader(f, delimiter=';')
            for row in reader:
                make, is_created = CarMake.objects.get_or_create(name=row[0])
                if is_created:
                    print(f'Make {row[0]} was added')
                _, is_created = CarModel.objects.get_or_create(name=row[1], make=make)
                if is_created:
                    print(f'Car model {row[0]} {row[1]} was added')

        left = self.default_new_offers - CarOffer.objects.count()
        car_models = list()
        if left:
            car_models = list(CarModel.objects.all())
        for _ in range(left):
            random_car_path = random.choice(car_file_names)
            instance = CarOffer.objects.create(
                used=random.random() > 0.5,
                price=1000 + 50000 * random.random(),
                mileage=1000 + 100000 * random.random(),
                number_of_owners=random.randint(1, 5),
                description='This is a test offer',
                car_model=random.choice(car_models),
                year=random.choice(year_choices)[1],
            )
            new_path = os.path.join(
                settings.MEDIA_ROOT,
                'images',
                str(instance.uid),
                os.path.basename(random_car_path)
            )
            os.makedirs(os.path.dirname(new_path), exist_ok=True)
            shutil.copyfile(random_car_path, new_path)
            path_to_write = os.path.join(
                'images',
                str(instance.uid),
                os.path.basename(random_car_path)
            )
            instance.image = path_to_write
            instance.save()
            print(f'A new offer {instance.id} was added')
