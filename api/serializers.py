from rest_framework import serializers
from rest_framework.reverse import reverse_lazy

from api.models import CarOffer


class CarOfferSerializerList(serializers.ModelSerializer):
    make = serializers.CharField(source='car_model.make.name')
    car_model = serializers.CharField(source='car_model.name')
    url = serializers.SerializerMethodField('generate_url')

    class Meta:
        model = CarOffer
        fields = ('id', 'make', 'car_model', 'used', 'price', 'url', 'image')

    def generate_url(self, obj):
        kwargs = {
            'pk': obj.id,
        }
        url = reverse_lazy('car-offer', kwargs=kwargs)
        return self.context['request'].build_absolute_uri(url)


class CarOfferSerializerRetrieve(serializers.ModelSerializer):
    make = serializers.CharField(source='car_model.make.name')
    car_model = serializers.CharField(source='car_model.name')

    class Meta:
        model = CarOffer
        exclude = ('uid',)


class CarOfferSerializerCreate(serializers.ModelSerializer):
    class Meta:
        model = CarOffer
        exclude = ('car_model_name', 'uid', 'id')
