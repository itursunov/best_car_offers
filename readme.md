###### `Thi is a test task implemented by Ibrokhim Tursunov

###### `How to run
Run the docker with command:

`docker-compose up`

Actions:
1. Migrations will be applied.
2. Tables will be populated with test data.
3. Test cases will be implemented.
4. Test server will be launched on the localhost with **port 8000**.

###### Endpoints

`/car-offers/?p=<page_number>` - GET method allows to retrieve all offers in desc. order. To get more records please use pagination.

`/car-offers/` - POST method allows to create a new offer.

`/car-offers/<id>/` - GET method allows to retrieve details regarding specific offer.
 
######  Run tests
In order to run test separately please use the command:

`docker-compose run car_offer_itursunov python manage.py test api.tests -v 2`

######  How to view results
I suggest you to open endpoints **in the browser (Google Chrome e.g.)** and test REST API using Django Rest Framework user interface.