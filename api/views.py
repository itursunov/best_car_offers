from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets

from api.models import CarOffer
from api.paginations import CarOfferListPagination
from api.serializers import (
    CarOfferSerializerList,
    CarOfferSerializerCreate,
    CarOfferSerializerRetrieve,
)


class CarOfferView(viewsets.ModelViewSet):
    queryset = CarOffer.objects.all().order_by('-pk')
    serializer_class = CarOfferSerializerRetrieve
    pagination_class = CarOfferListPagination

    def get_serializer_class(self):
        if self.action in {'create', 'update'}:
            return CarOfferSerializerCreate
        if self.action == 'retrieve':
            return CarOfferSerializerRetrieve
        return CarOfferSerializerList

    def retrieve(self, request, pk=None):
        instance = self.get_object()
        return Response(self.serializer_class(instance).data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = self.get_object()
        serializer = self.serializer_class(instance, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(
                self.serializer_class(instance).data, status=status.HTTP_201_CREATED
            )
        return Response(status=status.HTTP_400_BAD_REQUEST)
