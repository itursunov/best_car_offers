import os
import uuid
import decimal
import datetime

from django.db import models
from django.core.validators import MinValueValidator


year_choices = [(y, y) for y in range(1940, datetime.date.today().year + 1)]
current_year = datetime.date.today().year


def image_path(instance, filename):
    return os.path.join('images', str(instance.uid), filename)


class CarMake(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=127)

    def __str__(self):
        return self.name


class CarModel(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=511)
    make = models.ForeignKey(CarMake, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f'{self.make} {self.name}'

    class Meta:
        unique_together = ('name', 'make')


class CarOffer(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False)
    used = models.BooleanField(null=True)
    price = models.DecimalField(
        max_digits=10, decimal_places=3, validators=[MinValueValidator(decimal.Decimal(0.01))]
    )
    mileage = models.PositiveIntegerField(default=1000)
    number_of_owners = models.SmallIntegerField(default=1)
    description = models.TextField()
    car_model = models.ForeignKey(
        CarModel, null=True, on_delete=models.SET_NULL, default=None
    )
    year = models.SmallIntegerField(
        choices=year_choices, default=current_year, verbose_name='year'
    )
    car_model_name = models.CharField(max_length=511, null=True, default=None)
    image = models.ImageField(upload_to=image_path, null=True, default=None)

    def save(self, *args, **kwargs):
        if self.car_model:
            self.car_model_name = self.car_model.name
        super().save(*args, **kwargs)
