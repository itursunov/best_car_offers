from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from api.views import CarOfferView


urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        'car-offers/', CarOfferView.as_view({'get': 'list', 'post': 'create'}), name='car-offers'
    ),
    path(
        'car-offers/<int:pk>/',
        CarOfferView.as_view({'get': 'retrieve', 'patch': 'update', 'delete': 'destroy'}),
        name='car-offer'
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
