from django.contrib import admin

from api.models import CarMake, CarModel, CarOffer


class CarMakeAdmin(admin.ModelAdmin):
    pass


class CarModelAdmin(admin.ModelAdmin):
    pass


class CarOfferAdmin(admin.ModelAdmin):
    pass


admin.site.register(CarMake, CarMakeAdmin)
admin.site.register(CarModel, CarModelAdmin)
admin.site.register(CarOffer, CarOfferAdmin)
