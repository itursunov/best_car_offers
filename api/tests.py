import os
import random
import json
import uuid

from django.test import TestCase, Client
from django.conf import settings
from api.models import CarOffer, CarModel, CarMake


FILE_PATH = os.path.join(settings.BASE_DIR, 'api', 'data', 'car_images', 'car_img_1.jpg')


class CarOfferModelTestCase(TestCase):
    def setUp(self):
        self.make = CarMake.objects.create(name='BMW')
        self.model = CarModel.objects.create(name='BMW model', make=self.make)
        self.offer = CarOffer.objects.create(
            car_model=self.model,
            used=True,
            price=12000,
            mileage=200000,
            number_of_owners=2,
            description='Short description of the car',
            year=2010,
        )

    def test_offer_save_car_model_name(self):
        model_name = self.model.name
        self.assertEqual(model_name, self.offer.car_model_name)

    def test_offer_has_car_model_name_after_car_model_delete(self):
        make = CarMake.objects.create(name='BMW')
        model = CarModel.objects.create(name='BMW model', make=make)
        model_name = model.name
        offer = CarOffer.objects.create(
            car_model=model,
            used=True,
            price=12000,
            mileage=200000,
            number_of_owners=2,
            description='Short description of the car',
            year=2010,
        )
        model.delete()
        self.assertEqual(model_name, offer.car_model_name)


class EndpointsTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        make = CarMake.objects.create(name='BMW')
        cls.model = CarModel.objects.create(name='BMW model', make=make)
        for _ in range(100):
            CarOffer.objects.create(
                car_model=cls.model,
                used=True,
                price=random.randint(5000, 300000),
                mileage=random.randint(1000, 200000),
                number_of_owners=random.randint(1, 5),
                description='Short description of the car',
                year=random.randint(1980, 2022),
            )

    def test_get_offers(self):
        response = self.client.get('/car-offers/')
        data = json.loads(response.content)
        self.assertEqual(data['count'], 100)
        self.assertEqual(response.status_code, 200)

    def test_get_offers_return_only_50_records(self):
        response = self.client.get('/car-offers/')
        data = json.loads(response.content)
        self.assertEqual(len(data['results']), 50)
        self.assertEqual(response.status_code, 200)

    def test_get_offers_return_records_in_desc_order(self):
        response = self.client.get('/car-offers/')
        data = json.loads(response.content)
        ids = [rec['id'] for rec in data['results']]
        self.assertGreater(ids[0], ids[-1])
        self.assertEqual(response.status_code, 200)

    def test_get_2nd_page_offers_return_new_records(self):
        response_one = self.client.get('/car-offers/')
        data_one = json.loads(response_one.content)
        first_page_ids = {rec['id'] for rec in data_one['results']}
        response_two = self.client.get('/car-offers/?p=2')
        data_two = json.loads(response_two.content)
        second_page_ids = {rec['id'] for rec in data_two['results']}
        self.assertGreater(len(first_page_ids), 0)
        self.assertGreater(len(second_page_ids), 0)
        common = first_page_ids & second_page_ids
        self.assertEqual(common, set())

    def test_get_offers_contains_only_required_fields(self):
        response = self.client.get('/car-offers/')
        data = json.loads(response.content)
        sample = data['results'][0]
        self.assertEqual(
            set(sample.keys()),
            {'id', 'image', 'price', 'car_model', 'url', 'make', 'used'}
        )

    def test_can_get_access_to_personal_offer_page(self):
        response = self.client.get('/car-offers/10/')
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['id'], 10)

    def test_get_specific_offer_page_contains_only_required_fields(self):
        response = self.client.get('/car-offers/10/')
        data = json.loads(response.content)
        self.assertEqual(
            set(data.keys()),
            {
                'id',
                'image',
                'price',
                'car_model',
                'make',
                'used',
                'number_of_owners',
                'car_model_name',
                'year',
                'mileage',
                'description',
             }
        )

    def test_can_create_new_offer_via_post(self):
        unique_val = str(uuid.uuid4())
        data = dict(
            car_model=self.model.id,
            used=True,
            price=99000,
            mileage=100000,
            number_of_owners=2,
            description=unique_val,
            year=2015,
        )
        response = self.client.post('/car-offers/', data)
        self.assertEqual(response.status_code, 201)
        offer = CarOffer.objects.filter(description=unique_val)
        self.assertGreater(offer.count(), 0)

    def test_can_delete_offer(self):
        offer = CarOffer.objects.create(
            car_model=self.model,
            used=True,
            price=12000,
            mileage=200000,
            number_of_owners=2,
            description='Short description of the car',
            year=2010,
        )
        id_ = offer.id
        self.client.delete(f'/car-offers/{id_}/')
        queryset = CarOffer.objects.filter(id=id_)
        self.assertEqual(queryset.count(), 0)

    def test_can_create_offer_with_image(self):
        unique_val = str(uuid.uuid4())
        with open(FILE_PATH, 'rb') as f:
            data = dict(
                car_model=self.model.id,
                used=True,
                price=99000,
                mileage=100000,
                number_of_owners=2,
                description=unique_val,
                year=2015,
                image=f,
            )
            response = self.client.post('/car-offers/', data)
        self.assertEqual(response.status_code, 201)
        instance = CarOffer.objects.get(description=unique_val)
        self.assertGreater(instance.image.path, '')
